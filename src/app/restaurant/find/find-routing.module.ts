import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FindPage } from './find.page';

const routes: Routes = [
  {
    path: '',
    component: FindPage
  },
  {
    path: 'restaurant-detail',
    loadChildren: () => import('./restaurant-detail/restaurant-detail.module').then( m => m.RestaurantDetailPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FindPageRoutingModule {}
