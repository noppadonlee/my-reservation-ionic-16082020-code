import { Component, OnInit } from '@angular/core';
import { Restaurant } from '../restaurant.model';
import { RestaurantService } from '../restaurant.service';

@Component({
  selector: 'app-find',
  templateUrl: './find.page.html',
  styleUrls: ['./find.page.scss'],
})
export class FindPage implements OnInit {
  loadedRestaurants: Restaurant[];

  constructor(private restaurantService:RestaurantService) { }

  ngOnInit() {
    this.loadedRestaurants = this.restaurantService.restaurants;
  }

}
