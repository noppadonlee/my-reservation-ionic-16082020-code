import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { NewReservationComponent } from 'src/app/reservation/new-reservation/new-reservation.component';
import { ActivatedRoute } from '@angular/router';
import { RestaurantService } from '../../restaurant.service';
import { Restaurant } from '../../restaurant.model';

@Component({
  selector: 'app-restaurant-detail',
  templateUrl: './restaurant-detail.page.html',
  styleUrls: ['./restaurant-detail.page.scss'],
})
export class RestaurantDetailPage implements OnInit {
  restaurant: Restaurant;
  constructor(
    private modalController:ModalController,
    private route: ActivatedRoute,
    private navController: NavController,
    private restaurantService: RestaurantService  
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe( paramMap => {
      if (!paramMap.has('restaurantId')) {
        this.navController.navigateBack('/restaurant/tabs/find');
        return;
      }
      this.restaurant = this.restaurantService.getRestaurant(paramMap.get('restaurantId'));
    });
  }

  onReservation() {
    this.modalController.create({
        component: NewReservationComponent
    })
    .then( modalElement => {
      modalElement.present();
    });
  }

}
