import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-new-reservation',
  templateUrl: './new-reservation.component.html',
  styleUrls: ['./new-reservation.component.scss'],
})
export class NewReservationComponent implements OnInit {
  @ViewChild('form') form: NgForm;

  constructor(private modalController:ModalController) { }

  ngOnInit() {}

  onCancel() {
    this.modalController.dismiss(null, 'cancel');
  }

  getMinYear() {
    return new Date().getFullYear();
  }

  checkValid() {
    const chooseDatetime = new Date(this.form.value['reservation_datetime']);
    return new Date() < chooseDatetime;
  }

  doneReserve() {
    
  }

}
