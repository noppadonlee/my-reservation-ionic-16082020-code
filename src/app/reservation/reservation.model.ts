export class Reservation {
    constructor(
        public id: string,
        public restaurantId: string,
        public userId: string,
        public restaurant_title: string,
        public restaurant_datetime: Date
    ) {}
}